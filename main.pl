use singleResult;
use Socket;
use strict;
use List::Util 'first'; 
use File::Slurp;
use REST::Neo4p;
REST::Neo4p->connect('http://127.0.0.1:7474');
my @allDevicesIP;
my @allDevicesName;
my @current;
my $name;
my $R;
my @done;
my @c;
my $ipin;
my $uniqueR;
my @NoSNMP;
my $unique;
my $rootIP="";
my $CDPIP="1.3.6.1.4.1.9.9.23.1.2.1.1.4";
my $CDPName="1.3.6.1.4.1.9.9.23.1.2.1.1.6";
my $sysName="1.3.6.1.2.1.1.5.0";
my $filename="NoSNMP";
my $n1;
my $n2;
my $node1;
my $node2;
my $hosts = REST::Neo4p::Index->new('node','hosts');
my $idx = REST::Neo4p->get_index_by_name('hosts','node');
my @plotted;
@NoSNMP=read_file($filename);
topology($rootIP);
sub topology{
	my @ips = @_;
	@current=undef;
	foreach my $ip (@ips){
		if (!($ip ~~ @NoSNMP)){
		 	$uniqueR =singleResult::SNMP_get($ip,$sysName);
		 

		 
			$unique=$uniqueR->{$sysName};
			
			@c=singleResult::SNMP_table($ip,$CDPIP);
			foreach my $ipinO (@c){
				$ipin=inet_ntoa( pack( "N", hex($ipinO) ) );


				$R=singleResult::SNMP_get($ipin,$sysName);
				

				$name=$R->{$sysName};
				
				if (!($name ~~ @done)){

				print $ip."(".$unique.") ==> ".$ipin."(".$name.")"."\n";
				push(@current,$ipin);
				
				if(!($unique ~~ @plotted)){
				$n1=REST::Neo4p::Node->new({ sysname => $unique, sysip => $ip,syscpu=>rand(100),sysmem=>rand(100)});
				$hosts->add_entry($n1, 'sysname'  => $unique);
                                 push(@plotted,$unique); }
				if(!($name ~~ @plotted)){
				$n2=REST::Neo4p::Node->new({ sysname => $name, sysip => $ipin,syscpu=>rand(100),sysmem=>rand(100)});
				$hosts->add_entry($n2, 'sysname'  => $name);
				                             push(@plotted,$name);     }
				if($name ne $unique){
				($node1) = $idx->find_entries(sysname => $name);
				($node2) =$idx->find_entries(sysname => $unique);   
				$node1->relate_to($node2, 'connected');
				}
			}

		}
			else{
				if (!(first { /$ipin/ } @NoSNMP)){
				open(DATA,"+>>NoSNMP") || die "Couldn't open file file.txt, $!";	
				print DATA $ipin."\n";
				push(@NoSNMP,$ipin);
				print "END ADDing in NoSNMP"."\n";
			}
			}


}
							


		
}
push(@done,$unique);
}
	
}

	topology(@current);


}
close DATA;	
}

