package singleResult;
use Exporter;
use vars qw($VERSION @ISA);
use strict;
use warnings;
my $result;
my @RE;
our $session;
$VERSION  = 1.00;
@ISA = qw(Exporter);
use Net::SNMP;
use Socket;
my $ipaddr;
sub SNMP_table{
	@RE=undef;
	my($host, $oid) = @_;
	

	($session,my $error) = Net::SNMP->session(Hostname =>$host,
											   Version=>3,
											   username=>$username,
											   authpassword=>$password,
											   timeout=>5);
	if($session){
		$result = $session->get_table($oid);
				}
	if(!defined($result)){
		($session,$error) = Net::SNMP->session(Hostname =>$host,
										   Version=>'2c',
										   community=>'public',
										   timeout=>1);
	if($session){
		$result = $session->get_table($oid);
				}

						}
	if(defined($result)){
		my %resultKey = %{$result};
		my $i=0;
		my $key;

		foreach $key (keys %resultKey){
			  $RE[$i]= $resultKey{$key}."\n";
			  $i++;
		   	}
	return @RE;


						}
	else {
		return 1;
		}

}


sub SNMP_get{
	$result=undef;
	my($host,$oid) = @_;

	my ($sessionG,$error) = Net::SNMP->session(Hostname => $host,
											   Version=>3,
											   username=>$username,
											   authpassword=>$password,
											   timeout=>1);
	if($sessionG){
		$result = $sessionG->get_request($oid);
	}
	if(!defined($result)){
		($sessionG,$error) = Net::SNMP->session(Hostname => $host,
											   Version=>'2c',
											   community=>'public',
											   timeout=>1);
	if($sessionG){
		$result = $sessionG->get_request($oid);
	}

	}
	if(defined($result)){
		return $result;
	}
	else {
		}
	
$sessionG.close();
}
1;
